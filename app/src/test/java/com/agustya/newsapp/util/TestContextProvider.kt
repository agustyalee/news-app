package com.agustya.newsapp.util

import com.agustya.newsapp.common.util.CoroutineContextProvider
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 15 October 2019
 */
class TestContextProvider : CoroutineContextProvider() {
    override val Main: CoroutineContext = Dispatchers.Unconfined
    override val IO: CoroutineContext = Dispatchers.Unconfined
}