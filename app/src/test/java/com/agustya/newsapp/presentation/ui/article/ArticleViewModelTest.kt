package com.agustya.newsapp.presentation.ui.article

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.agustya.newsapp.common.util.UseCaseResult
import com.agustya.newsapp.data.entity.Source
import com.agustya.newsapp.data.entity.response.ArticlesResponse
import com.agustya.newsapp.data.repository.ArticlesRepository
import com.agustya.newsapp.util.TestContextProvider
import com.agustya.newsapp.util.TestCoroutineRule
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import kotlin.test.assertNotNull

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 15 October 2019
 */
class ArticleViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var articlesRepository: ArticlesRepository

    private lateinit var viewModel: ArticleViewModel
    private lateinit var source: Source
    private val page = 1

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = ArticleViewModel(articlesRepository, TestContextProvider())
        val sourceString = "{\n" +
                "\t\t\t\t\"id\": \"abc-news-au\",\n" +
                "\t\t\t\t\"name\": \"ABC News (AU)\",\n" +
                "\t\t\t\t\"description\": \"Australia's most trusted source of local, national and world news. Comprehensive, independent, in-depth analysis, the latest business, sport, weather and more.\",\n" +
                "\t\t\t\t\"url\": \"http://www.abc.net.au/news\",\n" +
                "\t\t\t\t\"category\": \"general\",\n" +
                "\t\t\t\t\"language\": \"en\",\n" +
                "\t\t\t\t\"country\": \"au\",\n" +
                "\t\t\t\t\"urlsToLogos\": {\n" +
                "\t\t\t\t\t\"small\": \"\",\n" +
                "\t\t\t\t\t\"medium\": \"\",\n" +
                "\t\t\t\t\t\"large\": \"\"\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t\"sortBysAvailable\": [\"top\"]\n" +
                "\t\t\t}"
        source = Gson().fromJson(sourceString, Source::class.java)
        viewModel.source = source
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test load articles with success response`() = testCoroutineRule.runBlockingTest {
        // given
        val gson = Gson()
        val articlesString = "{\n" +
                "\t\"status\": \"ok\",\n" +
                "\t\"source\": \"abc-news-au\",\n" +
                "\t\"sortBy\": \"top\",\n" +
                "\t\"articles\": [{\n" +
                "\t\t\"author\": \"https://www.abc.net.au/news/joanna-menagh/4421090\",\n" +
                "\t\t\"title\": \"'Commando' knife killer jailed for life for stabbing mother-of-two in courthouse\",\n" +
                "\t\t\"description\": \"Paul Gary Turner used his training in commando knife skills to deliberately and intentionally stab the mother of his two children to death in a Perth court complex\",\n" +
                "\t\t\"url\": \"http://www.abc.net.au/news/2019-10-15/joondalup-courthouse-killer-paul-turner-jailed-for-life/11603330\",\n" +
                "\t\t\"urlToImage\": \"https://www.abc.net.au/news/image/8139122-16x9-700x394.jpg\",\n" +
                "\t\t\"publishedAt\": \"2019-10-15T03:29:00Z\"\n" +
                "\t}, {\n" +
                "\t\t\"author\": null,\n" +
                "\t\t\"title\": \"ATO scammers turn up at house dressed as police with eftpos machine\",\n" +
                "\t\t\"description\": \"Adelaide police say two men turned up to a victim's house with an eftpos machine demanding money after earlier calling him pretending to be from the tax office.\",\n" +
                "\t\t\"url\": \"http://www.abc.net.au/news/2019-10-15/ato-scammers-turn-up-at-house-with-eftpos-machine/11603144\",\n" +
                "\t\t\"urlToImage\": \"https://www.abc.net.au/news/image/3204352-16x9-700x394.jpg\",\n" +
                "\t\t\"publishedAt\": \"2019-10-15T00:29:31Z\"\n" +
                "\t}, {\n" +
                "\t\t\"author\": \"Eden Hynninen\",\n" +
                "\t\t\"title\": \"Woman deported for smuggling 4.6kg of uncooked pork into Sydney Airport\",\n" +
                "\t\t\"description\": \"Australia's biosecurity laws have tightened following the threat of African swine fever.\",\n" +
                "\t\t\"url\": \"http://www.abc.net.au/news/rural/2019-10-15/woman-deported-for-smuggling-uncooked-pork/11603336\",\n" +
                "\t\t\"urlToImage\": \"https://www.abc.net.au/cm/rimage/11603286-16x9-large.png?v=5\",\n" +
                "\t\t\"publishedAt\": \"2019-10-15T01:49:31Z\"\n" +
                "\t}]\n" +
                "}"
        val articlesResponse = gson.fromJson<ArticlesResponse>(articlesString, ArticlesResponse::class.java)
        Mockito.`when`(articlesRepository.getArticles(source, page)).thenReturn(UseCaseResult.Success(articlesResponse))

        // when
        viewModel.loadArticles()

        // then
        Mockito.verify(articlesRepository, Mockito.times(1)).getArticles(source, page)
        assertNotNull(viewModel.articlesResponse.value)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test load articles with exception`() = testCoroutineRule.runBlockingTest {
        // given
        Mockito.`when`(articlesRepository.getArticles(source, page)).thenReturn(UseCaseResult.Exception(Exception("some error message")))

        // when
        viewModel.loadArticles()

        // then
        Mockito.verify(articlesRepository, Mockito.times(1)).getArticles(source, page)
        assertNotNull(viewModel.errorMessageEvent.value)
    }
}