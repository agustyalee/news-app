package com.agustya.newsapp.presentation.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.agustya.newsapp.common.util.UseCaseResult
import com.agustya.newsapp.data.entity.response.SourcesResponse
import com.agustya.newsapp.data.repository.SourcesRepository
import com.agustya.newsapp.util.TestContextProvider
import com.agustya.newsapp.util.TestCoroutineRule
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import kotlin.test.assertNotNull

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 15 October 2019
 */
class MainViewModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Mock
    private lateinit var sourcesRepository: SourcesRepository

    private lateinit var viewModel: MainViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel(sourcesRepository, TestContextProvider())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test load sources with success response`() = testCoroutineRule.runBlockingTest {
        // given
        val gson = Gson()
        val sourcesString = "{\n" +
                "\t\"status\": \"ok\",\n" +
                "\t\"sources\": [{\n" +
                "\t\t\"id\": \"abc-news-au\",\n" +
                "\t\t\"name\": \"ABC News (AU)\",\n" +
                "\t\t\"description\": \"Australia's most trusted source of local, national and world news. Comprehensive, independent, in-depth analysis, the latest business, sport, weather and more.\",\n" +
                "\t\t\"url\": \"http://www.abc.net.au/news\",\n" +
                "\t\t\"category\": \"general\",\n" +
                "\t\t\"language\": \"en\",\n" +
                "\t\t\"country\": \"au\",\n" +
                "\t\t\"urlsToLogos\": {\n" +
                "\t\t\t\"small\": \"\",\n" +
                "\t\t\t\"medium\": \"\",\n" +
                "\t\t\t\"large\": \"\"\n" +
                "\t\t},\n" +
                "\t\t\"sortBysAvailable\": [\"top\"]\n" +
                "\t}, {\n" +
                "\t\t\"id\": \"al-jazeera-english\",\n" +
                "\t\t\"name\": \"Al Jazeera English\",\n" +
                "\t\t\"description\": \"News, analysis from the Middle East and worldwide, multimedia and interactives, opinions, documentaries, podcasts, long reads and broadcast schedule.\",\n" +
                "\t\t\"url\": \"http://www.aljazeera.com\",\n" +
                "\t\t\"category\": \"general\",\n" +
                "\t\t\"language\": \"en\",\n" +
                "\t\t\"country\": \"us\",\n" +
                "\t\t\"urlsToLogos\": {\n" +
                "\t\t\t\"small\": \"\",\n" +
                "\t\t\t\"medium\": \"\",\n" +
                "\t\t\t\"large\": \"\"\n" +
                "\t\t},\n" +
                "\t\t\"sortBysAvailable\": [\"top\"]\n" +
                "\t}]\n" +
                "}"
        val sourcesResponse = gson.fromJson<SourcesResponse>(sourcesString, SourcesResponse::class.java)
        Mockito.`when`(sourcesRepository.getSources(Mockito.anyInt())).thenReturn(UseCaseResult.Success(sourcesResponse))

        // when
        viewModel.loadSources()

        // then
        Mockito.verify(sourcesRepository, Mockito.times(1)).getSources(Mockito.anyInt())
        assertNotNull(viewModel.sourcesResponse.value)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test load sources with exception`() = testCoroutineRule.runBlockingTest {
        // given
        Mockito.`when`(sourcesRepository.getSources(Mockito.anyInt())).thenReturn(UseCaseResult.Exception(Exception("some error message")))

        // when
        viewModel.loadSources()

        // then
        Mockito.verify(sourcesRepository, Mockito.times(1)).getSources(Mockito.anyInt())
        assertNotNull(viewModel.errorMessageEvent.value)
    }
}