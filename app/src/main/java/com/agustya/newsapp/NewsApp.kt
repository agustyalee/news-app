package com.agustya.newsapp

import android.app.Application
import com.agustya.newsapp.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
class NewsApp : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())

        startKoin {
            androidContext(this@NewsApp)
            modules(appModule)
        }
    }
}