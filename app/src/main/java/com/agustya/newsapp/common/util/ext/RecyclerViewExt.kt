package com.agustya.newsapp.common.util.ext

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 15 October 2019
 */
fun RecyclerView.setup(recyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>, orientation: Int = RecyclerView.VERTICAL) {
    if (layoutManager == null) {
        layoutManager = LinearLayoutManager(context, orientation, false)
    }
    if (adapter == null) {
        adapter = recyclerAdapter
    }
}