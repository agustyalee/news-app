package com.agustya.newsapp.common.util

import org.json.JSONObject
import retrofit2.HttpException
import timber.log.Timber

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
sealed class UseCaseResult<out T : Any> {
    class Success<out T : Any>(val data: T) : UseCaseResult<T>()
    class Error<out T : Any>(val data: T) : UseCaseResult<T>()
    class Exception(val exception: Throwable) : UseCaseResult<Nothing>()
}

inline fun <T : Any> UseCaseResult<T>.ifSucceeded(handler: (data: T) -> Unit): UseCaseResult<T> {
    (this as? UseCaseResult.Success)?.data?.let { handler(it) }
    return this
}

//inline fun <T : Any> UseCaseResult<T>.ifError(handler: (errorMessage: String) -> Unit): UseCaseResult<T> {
//    (this as? UseCaseResult.Error)?.data?.let {
//        val baseResponse = it as BaseResponse
//        Timber.d("${baseResponse.status.orEmpty()} ${baseResponse.message.orEmpty()}")
//        handler(baseResponse.message.orEmpty())
//    }
//    return this
//}

inline fun <T : Any> UseCaseResult<T>.ifException(handler: (httpCode: Int, errorMessage: String) -> Unit): UseCaseResult<T> {
    (this as? UseCaseResult.Exception)?.let {
        var httpCode = 0
        var status = ""
        var errorMessage = ""

        if (it.exception is HttpException) {
            httpCode = it.exception.code()

            try {
                it.exception.response()?.errorBody()?.let { responseBody ->
                    val jsonObject = JSONObject(responseBody.string())
                    if (jsonObject.has("status")) {
                        status = jsonObject.get("status").toString()
                    }
                    if (jsonObject.has("message")) {
                        errorMessage = jsonObject.get("message").toString()
                    }
                }
            } catch (e: Exception) {
                if (!it.exception.message().isNullOrBlank()) {
                    errorMessage = it.exception.message()
                } else if (!it.exception.response()?.message().isNullOrBlank()) {
                    errorMessage = it.exception.response()?.message().orEmpty()
                }
            }
        } else {
            if (!it.exception.message.isNullOrBlank()) {
                errorMessage = it.exception.message.orEmpty()
            }
        }
        Timber.d("$httpCode $status $errorMessage")
        handler(httpCode, errorMessage)
    }
    return this
}