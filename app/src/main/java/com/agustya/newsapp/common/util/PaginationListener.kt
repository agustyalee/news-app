package com.agustya.newsapp.common.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 20 September 2019
 */
abstract class PaginationListener(private val pageSize: Int) : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val manager = recyclerView.layoutManager
        val visibleItemCount = manager?.childCount ?: 0
        val totalItemCount = manager?.itemCount ?: 0
        val firstVisibleItemPosition = (manager as? LinearLayoutManager)?.findFirstVisibleItemPosition()
                ?: 0

        if (!isLoading() && isLoadMore()) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                    && firstVisibleItemPosition >= 0 && totalItemCount >= pageSize) {
                loadMoreItems()
            }
        }
    }

    protected abstract fun loadMoreItems()

    abstract fun isLoadMore(): Boolean

    abstract fun isLoading(): Boolean
}