package com.agustya.newsapp.common.util.base

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
open class BaseViewModel : ViewModel() {
    protected var page = 1
    protected var isLoading = false

    protected var _isLoadMore = MutableLiveData<Boolean>().apply { value = false }
    val isLoadMore: LiveData<Boolean> = _isLoadMore

    protected val _loadingVisibility = MutableLiveData<Int>().apply { value = View.VISIBLE }
    val loadingVisibility: LiveData<Int> = _loadingVisibility
}