package com.agustya.newsapp.di

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
val appModule = listOf(networkModule, repositoryModule, viewModelModule)