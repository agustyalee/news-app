package com.agustya.newsapp.di

import com.agustya.newsapp.common.util.CoroutineContextProvider
import com.agustya.newsapp.presentation.ui.article.ArticleViewModel
import com.agustya.newsapp.presentation.ui.detail.DetailViewModel
import com.agustya.newsapp.presentation.ui.main.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
val viewModelModule = module {
    viewModel { MainViewModel(get(), CoroutineContextProvider()) }
    viewModel { ArticleViewModel(get(), CoroutineContextProvider()) }
    viewModel { DetailViewModel() }
}