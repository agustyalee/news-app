package com.agustya.newsapp.di

import com.agustya.newsapp.data.repository.ArticlesRepository
import com.agustya.newsapp.data.repository.ArticlesRepositoryImpl
import com.agustya.newsapp.data.repository.SourcesRepository
import com.agustya.newsapp.data.repository.SourcesRepositoryImpl
import org.koin.dsl.module

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
val repositoryModule = module {
    factory<SourcesRepository> { SourcesRepositoryImpl(get()) }
    factory<ArticlesRepository> { ArticlesRepositoryImpl(get()) }
}