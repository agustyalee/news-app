package com.agustya.newsapp.di

import com.agustya.newsapp.data.Const
import com.agustya.newsapp.data.api.ApiService
import com.agustya.newsapp.data.api.ApiUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
val networkModule = module {
    single { createHttpLoggingInterceptor() }
    single { createOkHttpClient(get()) }
    single { createRetrofit(get()) }
    single { createApiService(get()) }
}

fun createHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val httpLoggingInterceptor = HttpLoggingInterceptor {
        Timber.d("retrofit $it")
    }
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return httpLoggingInterceptor
}

fun createOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder()
            .connectTimeout(Const.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(Const.CONNECTION_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(HeaderInterceptor())
            .addInterceptor(httpLoggingInterceptor)
            .build()
}

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
                .addHeader("Authorization", "Bearer ${Const.API_KEY}")
                .build()
        return chain.proceed(request)
    }
}

fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
            .baseUrl(ApiUrl.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}

fun createApiService(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
}