package com.agustya.newsapp.presentation.ui.detail

import android.graphics.Bitmap
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import com.agustya.newsapp.common.util.base.BaseViewModel

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
class DetailViewModel : BaseViewModel() {
    fun getWebViewClient() = object : WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            _loadingVisibility.value = View.VISIBLE
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            _loadingVisibility.value = View.GONE
        }
    }
}