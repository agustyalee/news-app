package com.agustya.newsapp.presentation.ui.article

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.agustya.newsapp.R
import com.agustya.newsapp.common.util.ext.setup
import com.agustya.newsapp.data.Const
import com.agustya.newsapp.data.entity.Article
import com.agustya.newsapp.data.entity.Source
import com.agustya.newsapp.databinding.ActivityArticleBinding
import com.agustya.newsapp.presentation.ui.detail.DetailActivity
import org.koin.android.viewmodel.ext.android.viewModel

class ArticleActivity : AppCompatActivity() {
    private val binding: ActivityArticleBinding by lazy {
        DataBindingUtil.setContentView<ActivityArticleBinding>(this, R.layout.activity_article)
    }
    private val viewModel: ArticleViewModel by viewModel()
    private lateinit var articleRecyclerAdapter: ArticleRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        parsingBundle()
        articleRecyclerAdapter = ArticleRecyclerAdapter {
            navigateTo(it)
        }
        setupViewModel()
        setupView()
        viewModel.loadArticles()
    }

    private fun parsingBundle() {
        intent?.extras?.run {
            val source: Source? = getParcelable(Const.Extra.SOURCE)
            viewModel.initBundle(source)
        }
    }

    private fun setupViewModel() {
        viewModel.articlesResponse.observe(this, Observer {
            articleRecyclerAdapter.addObjects(it.articles)
        })
        viewModel.errorMessageEvent.observe(this, Observer {
            Toast.makeText(this, it.orEmpty(), Toast.LENGTH_SHORT).show()
        })
        viewModel.isLoadMore.observe(this, Observer {
            if (it) {
                articleRecyclerAdapter.addLoader()
            } else {
                articleRecyclerAdapter.removeLoader()
            }
        })
        viewModel.searchString.observe(this, Observer {
            articleRecyclerAdapter.ItemFilter().filter(it)
        })
    }

    private fun setupView() {
        with(binding) {
            lifecycleOwner = this@ArticleActivity
            viewModel = this@ArticleActivity.viewModel

            rvItem.setup(articleRecyclerAdapter)
            rvItem.addItemDecoration(DividerItemDecoration(rvItem.context, DividerItemDecoration.VERTICAL))
            rvItem.addOnScrollListener(this@ArticleActivity.viewModel.getPaginationListener())
        }
    }

    private fun navigateTo(article: Article) {
        Intent(this, DetailActivity::class.java).apply {
            putExtra(Const.Extra.ARTICLE, article)
        }.also { startActivity(it) }
    }
}