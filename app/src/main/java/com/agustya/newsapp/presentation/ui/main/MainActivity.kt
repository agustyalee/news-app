package com.agustya.newsapp.presentation.ui.main

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.agustya.newsapp.R
import com.agustya.newsapp.common.util.ext.setup
import com.agustya.newsapp.data.Const
import com.agustya.newsapp.data.entity.Source
import com.agustya.newsapp.databinding.ActivityMainBinding
import com.agustya.newsapp.presentation.ui.article.ArticleActivity
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val binding: ActivityMainBinding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }
    private val viewModel: MainViewModel by viewModel()
    private lateinit var sourceRecyclerAdapter: SourceRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sourceRecyclerAdapter = SourceRecyclerAdapter {
            navigateTo(it)
        }
        setupViewModel()
        setupView()
        viewModel.loadSources()
    }

    private fun setupViewModel() {
        viewModel.sourcesResponse.observe(this, Observer {
            sourceRecyclerAdapter.addObjects(it.sources)
        })
        viewModel.errorMessageEvent.observe(this, Observer {
            Toast.makeText(this, it.orEmpty(), Toast.LENGTH_SHORT).show()
        })
        viewModel.isLoadMore.observe(this, Observer {
            if (it) {
                sourceRecyclerAdapter.addLoader()
            } else {
                sourceRecyclerAdapter.removeLoader()
            }
        })
        viewModel.searchString.observe(this, Observer {
            sourceRecyclerAdapter.ItemFilter().filter(it)
        })
    }

    private fun setupView() {
        with(binding) {
            lifecycleOwner = this@MainActivity
            viewModel = this@MainActivity.viewModel

            rvItem.setup(sourceRecyclerAdapter)
            rvItem.addItemDecoration(DividerItemDecoration(rvItem.context, DividerItemDecoration.VERTICAL))
            rvItem.addOnScrollListener(this@MainActivity.viewModel.getPaginationListener())
        }
    }

    private fun navigateTo(source: Source) {
        Intent(this, ArticleActivity::class.java).apply {
            putExtra(Const.Extra.SOURCE, source)
        }.also { startActivity(it) }
    }
}