package com.agustya.newsapp.presentation.ui.detail

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.agustya.newsapp.R
import com.agustya.newsapp.data.Const
import com.agustya.newsapp.data.entity.Article
import com.agustya.newsapp.databinding.ActivityDetailBinding
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
class DetailActivity : AppCompatActivity() {
    private val binding: ActivityDetailBinding by lazy {
        DataBindingUtil.setContentView<ActivityDetailBinding>(this, R.layout.activity_detail)
    }
    private val viewModel: DetailViewModel by viewModel()
    private var article: Article? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        parsingBundle()
        setupView()
        setupWebView()
        loadContent()
    }

    private fun loadContent() {
        article?.url?.let {
            binding.wvContent.loadUrl(it)
        }
    }

    private fun setupView() {
        with(binding) {
            lifecycleOwner = this@DetailActivity
            viewModel = this@DetailActivity.viewModel
        }
    }

    private fun parsingBundle() {
        intent?.extras?.run {
            article = getParcelable(Const.Extra.ARTICLE)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        binding.wvContent.settings.apply {
            javaScriptEnabled = true
            domStorageEnabled = true
        }
        binding.wvContent.webViewClient = viewModel.getWebViewClient()
    }
}