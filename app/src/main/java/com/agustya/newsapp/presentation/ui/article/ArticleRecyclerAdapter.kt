package com.agustya.newsapp.presentation.ui.article

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.agustya.newsapp.R
import com.agustya.newsapp.data.entity.Article
import com.agustya.newsapp.databinding.RecyclerItemArticleBinding
import com.agustya.newsapp.presentation.ui.common.viewholder.ProgressViewHolder
import com.agustya.newsapp.presentation.ui.main.ITEM
import com.agustya.newsapp.presentation.ui.main.LOADER
import com.bumptech.glide.Glide
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
class ArticleRecyclerAdapter(private val objects: ArrayList<Article> = arrayListOf(),
                             private var filteredObjects: ArrayList<Article> = arrayListOf(),
                             private val onItemClicked: (Article) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var isLoaderVisible = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM) {
            ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                    R.layout.recycler_item_article, parent, false))
        } else {
            ProgressViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycler_item_progress_bar, parent, false))
        }
    }

    fun addObjects(items: ArrayList<Article>) {
        objects.addAll(items)
        filteredObjects.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoaderVisible) {
            val item = filteredObjects[position]
            if (item.title.isNullOrBlank() && position == objects.size - 1) {
                LOADER
            } else {
                ITEM
            }
        } else {
            ITEM
        }
    }

    fun addLoader() {
        isLoaderVisible = true
        filteredObjects.add(Article())
        notifyItemInserted(filteredObjects.size - 1)
    }

    fun removeLoader() {
        if (isLoaderVisible) {
            isLoaderVisible = false
            val position = filteredObjects.size - 1
            if (position != -1) {
                filteredObjects.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    override fun getItemCount() = filteredObjects.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.onBind(position)
        }
    }

    inner class ViewHolder(private val binding: RecyclerItemArticleBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(position: Int) {
            val item = filteredObjects[position]
            itemView.setOnClickListener { onItemClicked.invoke(item) }
            with(binding) {
                Glide.with(ivItem.context).load(item.urlToImage.orEmpty()).into(ivItem)
                tvTitle.text = item.title.orEmpty()
                tvDescription.text = item.description.orEmpty()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    inner class ItemFilter : Filter() {
        override fun performFiltering(charSequence: CharSequence): FilterResults {
            val filterString = charSequence.toString().toLowerCase(Locale.getDefault())
            val results = FilterResults()
            val nList = ArrayList<Article>()

            objects.forEach {
                val filterableName = it.title?.toLowerCase(Locale.getDefault())
                if (filterableName?.contains(filterString) == true) {
                    nList.add(it)
                }
            }

            results.values = nList
            results.count = nList.size
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            filteredObjects = results.values as ArrayList<Article>
            notifyDataSetChanged()
        }
    }
}