package com.agustya.newsapp.presentation.ui.main

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.agustya.newsapp.common.util.CoroutineContextProvider
import com.agustya.newsapp.common.util.PaginationListener
import com.agustya.newsapp.common.util.SingleLiveEvent
import com.agustya.newsapp.common.util.base.BaseViewModel
import com.agustya.newsapp.common.util.ifException
import com.agustya.newsapp.common.util.ifSucceeded
import com.agustya.newsapp.data.entity.response.SourcesResponse
import com.agustya.newsapp.data.repository.SourcesRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
class MainViewModel(private val sourcesRepository: SourcesRepository,
                    private val coroutineContextProvider: CoroutineContextProvider) : BaseViewModel() {

    private val _sourcesResponse = MutableLiveData<SourcesResponse>()
    val sourcesResponse: LiveData<SourcesResponse> = _sourcesResponse

    val searchString = MutableLiveData<String>()
    val errorMessageEvent = SingleLiveEvent<String>()

    fun loadSources() {
        viewModelScope.launch {
            if (page == 1) {
                _loadingVisibility.value = View.VISIBLE
            }
            isLoading = true
            val result = withContext(coroutineContextProvider.IO) {
                sourcesRepository.getSources(page)
            }
            if (page == 1) {
                _loadingVisibility.value = View.GONE
            }
            isLoading = false
            // to remove load more after adding
            if (_isLoadMore.value == true) {
                _isLoadMore.value = false
            }
            result.ifSucceeded {
                _sourcesResponse.value = it
                if (it.sources.size >= PAGE_SIZE) {
                    _isLoadMore.value = true
                    page += 1
                } else {
                    _isLoadMore.value = false
                }
            }.ifException { _, errorMessage ->
                errorMessageEvent.value = errorMessage
            }
        }
    }

    fun getPaginationListener() = object : PaginationListener(PAGE_SIZE) {
        override fun loadMoreItems() {
            loadSources()
        }

        override fun isLoadMore() = _isLoadMore.value ?: false
        override fun isLoading() = isLoading
    }
}

// based on all items on sources api
private const val PAGE_SIZE = 67