package com.agustya.newsapp.presentation.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.agustya.newsapp.R
import com.agustya.newsapp.data.entity.Source
import com.agustya.newsapp.databinding.RecyclerItemSourceBinding
import com.agustya.newsapp.presentation.ui.common.viewholder.ProgressViewHolder
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
class SourceRecyclerAdapter(private val objects: ArrayList<Source> = arrayListOf(),
                            private var filteredObjects: ArrayList<Source> = arrayListOf(),
                            private val onItemClicked: (Source) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {
    private var isLoaderVisible = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ITEM) {
            ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                    R.layout.recycler_item_source, parent, false))
        } else {
            ProgressViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycler_item_progress_bar, parent, false))
        }
    }

    fun addObjects(items: ArrayList<Source>) {
        objects.addAll(items)
        filteredObjects.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoaderVisible) {
            val item = filteredObjects[position]
            if (item.name.isNullOrBlank() && position == filteredObjects.size - 1) {
                LOADER
            } else {
                ITEM
            }
        } else {
            ITEM
        }
    }

    fun addLoader() {
        isLoaderVisible = true
        filteredObjects.add(Source())
        notifyItemInserted(filteredObjects.size - 1)
    }

    fun removeLoader() {
        if (isLoaderVisible) {
            isLoaderVisible = false
            val position = filteredObjects.size - 1
            if (position != -1) {
                filteredObjects.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    override fun getItemCount() = filteredObjects.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.onBind(position)
        }
    }

    inner class ViewHolder(private val binding: RecyclerItemSourceBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(position: Int) {
            val item = filteredObjects[position]
            itemView.setOnClickListener { onItemClicked.invoke(item) }
            with(binding) {
                tvName.text = item.name.orEmpty()
                tvDescription.text = item.description.orEmpty()
            }
        }
    }

    override fun getFilter(): Filter = ItemFilter()

    @Suppress("UNCHECKED_CAST")
    inner class ItemFilter : Filter() {
        override fun performFiltering(charSequence: CharSequence): FilterResults {
            val filterString = charSequence.toString().toLowerCase(Locale.getDefault())
            val results = FilterResults()
            val nList = ArrayList<Source>()

            objects.forEach {
                val filterableName = it.name?.toLowerCase(Locale.getDefault())
                if (filterableName?.contains(filterString) == true) {
                    nList.add(it)
                }
            }

            results.values = nList
            results.count = nList.size
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            filteredObjects = results.values as ArrayList<Source>
            notifyDataSetChanged()
        }
    }
}

const val ITEM = 0
const val LOADER = 1