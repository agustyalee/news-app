package com.agustya.newsapp.presentation.ui.article

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.agustya.newsapp.common.util.CoroutineContextProvider
import com.agustya.newsapp.common.util.PaginationListener
import com.agustya.newsapp.common.util.SingleLiveEvent
import com.agustya.newsapp.common.util.base.BaseViewModel
import com.agustya.newsapp.common.util.ifException
import com.agustya.newsapp.common.util.ifSucceeded
import com.agustya.newsapp.data.entity.Source
import com.agustya.newsapp.data.entity.response.ArticlesResponse
import com.agustya.newsapp.data.repository.ArticlesRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
class ArticleViewModel(private val articlesRepository: ArticlesRepository,
                       private val coroutineContextProvider: CoroutineContextProvider) : BaseViewModel() {

    private val _articlesResponse = MutableLiveData<ArticlesResponse>()
    val articlesResponse = _articlesResponse

    val searchString = MutableLiveData<String>()
    val errorMessageEvent = SingleLiveEvent<String>()

    var source: Source? = null

    fun initBundle(source: Source?) {
        this.source = source
    }

    fun loadArticles() {
        source?.let {
            viewModelScope.launch {
                if (page == 1) {
                    _loadingVisibility.value = View.VISIBLE
                }
                isLoading = true
                val result = withContext(coroutineContextProvider.IO) {
                    articlesRepository.getArticles(it, page)
                }
                if (page == 1) {
                    _loadingVisibility.value = View.GONE
                }
                isLoading = false
                // to remove load more after adding
                if (_isLoadMore.value == true) {
                    _isLoadMore.value = false
                }
                result.ifSucceeded {
                    _articlesResponse.value = it
                    if (it.articles.size >= PAGE_SIZE) {
                        _isLoadMore.value = true
                        page += 1
                    } else {
                        _isLoadMore.value = false
                    }
                }.ifException { _, errorMessage ->
                    errorMessageEvent.value = errorMessage
                }
            }
        }
    }

    fun getPaginationListener() = object : PaginationListener(PAGE_SIZE) {
        override fun loadMoreItems() {
            loadArticles()
        }

        override fun isLoadMore() = _isLoadMore.value ?: false
        override fun isLoading() = isLoading
    }
}

// based on articles api
private const val PAGE_SIZE = 10