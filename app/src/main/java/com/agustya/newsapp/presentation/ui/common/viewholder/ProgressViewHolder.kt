package com.agustya.newsapp.presentation.ui.common.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 15 October 2019
 */
class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)