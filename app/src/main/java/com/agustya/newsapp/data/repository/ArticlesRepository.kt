package com.agustya.newsapp.data.repository

import com.agustya.newsapp.common.util.UseCaseResult
import com.agustya.newsapp.data.api.ApiService
import com.agustya.newsapp.data.entity.Source
import com.agustya.newsapp.data.entity.response.ArticlesResponse

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
interface ArticlesRepository {
    suspend fun getArticles(source: Source, page: Int): UseCaseResult<ArticlesResponse>
}

class ArticlesRepositoryImpl(private val apiService: ApiService) : ArticlesRepository {
    override suspend fun getArticles(source: Source, page: Int): UseCaseResult<ArticlesResponse> {
        return try {
            val result = apiService.getArticles(source.id.orEmpty())
            UseCaseResult.Success(result)
        } catch (e: Exception) {
            UseCaseResult.Exception(e)
        }
    }
}