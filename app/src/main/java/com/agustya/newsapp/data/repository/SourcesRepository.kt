package com.agustya.newsapp.data.repository

import com.agustya.newsapp.common.util.UseCaseResult
import com.agustya.newsapp.data.api.ApiService
import com.agustya.newsapp.data.entity.response.SourcesResponse

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
interface SourcesRepository {
    suspend fun getSources(page: Int): UseCaseResult<SourcesResponse>
}

class SourcesRepositoryImpl(private val apiService: ApiService) : SourcesRepository {
    override suspend fun getSources(page: Int): UseCaseResult<SourcesResponse> {
        return try {
            // use pagination in here
            val result = apiService.getSources()
            UseCaseResult.Success(result)
        } catch (e: Exception) {
            UseCaseResult.Exception(e)
        }
    }
}