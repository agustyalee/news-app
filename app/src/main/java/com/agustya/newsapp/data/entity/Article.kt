package com.agustya.newsapp.data.entity

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
@Keep
@Parcelize
data class Article(val author: String? = null,
                   val title: String? = null,
                   val description: String? = null,
                   val url: String? = null,
                   val urlToImage: String? = null,
                   val publishedAt: String? = null) : Parcelable