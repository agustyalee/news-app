package com.agustya.newsapp.data

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
object Const {
    const val CONNECTION_TIME_OUT = 90L
    const val API_KEY = "51d6ab0865d04b408c95599e7aaf1818"

    object Extra {
        const val SOURCE = "source"
        const val ARTICLE = "article"
    }
}
