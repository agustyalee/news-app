package com.agustya.newsapp.data.api

import com.agustya.newsapp.data.entity.response.ArticlesResponse
import com.agustya.newsapp.data.entity.response.SourcesResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
interface ApiService {
    @GET(ApiUrl.GET_SOURCES)
    suspend fun getSources(): SourcesResponse

    @GET(ApiUrl.GET_ARTICLES)
    suspend fun getArticles(@Query("source") source: String): ArticlesResponse


}