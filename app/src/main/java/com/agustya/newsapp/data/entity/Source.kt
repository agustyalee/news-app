package com.agustya.newsapp.data.entity

import android.os.Parcelable
import androidx.annotation.Keep
import kotlinx.android.parcel.Parcelize

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
@Keep
@Parcelize
data class Source(val id: String? = null,
                  val name: String? = null,
                  val description: String? = null,
                  val url: String? = null,
                  val category: String? = null,
                  val language: String? = null,
                  val country: String? = null) : Parcelable