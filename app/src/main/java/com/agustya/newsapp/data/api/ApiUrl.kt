package com.agustya.newsapp.data.api

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
object ApiUrl {
    const val BASE_URL = "https://newsapi.org"
    const val GET_SOURCES = "/v1/sources"
    const val GET_ARTICLES = "/v1/articles"
}