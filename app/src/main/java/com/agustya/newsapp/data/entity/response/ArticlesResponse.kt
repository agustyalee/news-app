package com.agustya.newsapp.data.entity.response

import androidx.annotation.Keep
import com.agustya.newsapp.data.entity.Article

/**
 * Created by Agustya (agustya.lee@ovo.id)
 * on 14 October 2019
 */
@Keep
data class ArticlesResponse(val articles: ArrayList<Article> = arrayListOf())